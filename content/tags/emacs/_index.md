+++
title = "Emacs"
date = 2022-12-23T22:08:00-06:00
draft = false
+++

Hard to define as it is, Emacs is a near-infinitely extensible text editor that's really just an _emacs-lisp_ interpreter (built on _C_). Within the same application, you can efficiently write code, write books, browse files, and much, much more.
