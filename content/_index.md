---
title: ""
description: ""
---

I study philosophy at an undergraduate level in the U.S. Here you will find a collection of my argued reflections and musings. While idiosyncratic, I hope they are thought-provoking reads.
