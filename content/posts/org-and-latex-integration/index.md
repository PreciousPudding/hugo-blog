+++
title = "Org and LaTeX integration"
author = ["Kristoffer Balintona"]
date = 2023-07-26T23:54:00-05:00
tags = ["Emacs"]
categories = ["Guides"]
draft = true
series = "Writing papers in org-mode"
+++

## My desired workflow {#my-desired-workflow}

Org-export is a means to convert org files into LaTeX files. The question this article explores is how I answer the following question: **How can I write my university essays in org and export them directly into a LaTeX PDF file without any further adjustments?** This means the following:

Basic formatting
: Elements such as line spacing, margins, font should be set automatically.

Citations
: In-text citations and bibliographies should be formatted automatically.

Flexibility with styles
: I should be able to switch between, e.g., MLA, APA, and Chicago Manual Style without any fuss.

Boilerplate text
: I want to do all of the above with very little to no boilerplate text in my org files (e.g. I shouldn't rely on many `#+LaTeX_header:` lines).

In short, I simply want to produce my final paper's PDF by calling a single org-export command. Here, I visit the portions of my configuration where I make accomplish such a workflow.

A note for STEM (Science, Technology, Engineering, and Mathematics) students: As a philosophy undergraduate, this article primarily centers on a subset of concerns STEM students would face in formatting their papers, since mathematics, tables, and other LaTeX features will be much more common. This is beyond my use case, but I hope that this article still has a use for those students. Additionally, there are many other resources available that cover similar use cases (link such sources later tk).


## Basic Formatting: `org-latex-classes` {#basic-formatting-org-latex-classes}


## Title pages: Creating an Org-export backend {#title-pages-creating-an-org-export-backend}

The most challenging hurdle is conveniently producing a formatted title page[^fn:1].

The most convenient method would be setting `org-latex-title-command`. With this, we can manually insert a formatted title page. However, this method is inflexible: different styles require different values for this variable.

A nearby alternative would be redefining the default `\maketitle` command that org already uses for every file. However, doing so would require additional `#+LaTeX_HEADER:` text.

Conveniently, we can leverage the already exist .sty file and redefine the `\maketitle` command there, instead.

The challenge, however, lies in how to get the requisite metadata from our org file. For instance, MLA requires four pieces of information in its title page:

1.  Author name
2.  Professor's name
3.  Course code and/or title
4.  Current date

Items (1) and (4) can be done simply. However, (2) and (3) are contingent on the exported essay. We can try making use of built-in org options such as `#+AUTHOR:` and `#+CREATOR:`, but this alone cannot provide a robust solution. **So, instead of trying to abuse existing options, I opt to create my own.**

Take a look at the docstring for `org-export-define-backend`:

<details>
<summary>Docstring for <code>org-export-define-backend</code></summary>
<div class="details">

<div class="verse">

(org-export-define-backend BACKEND TRANSCODERS &amp;rest BODY)<br />
<br />
Define a new backend BACKEND.<br />
<br />
TRANSCODERS is an alist between object or element types and<br />
functions handling them.<br />
<br />
These functions should return a string without any trailing<br />
space, or nil.  They must accept three arguments: the object or<br />
element itself, its contents or nil when it isn’t recursive and<br />
the property list used as a communication channel.<br />
<br />
Contents, when not nil, are stripped from any global indentation<br />
(although the relative one is preserved).  They also always end<br />
with a single newline character.<br />
<br />
If, for a given type, no function is found, that element or<br />
object type will simply be ignored, along with any blank line or<br />
white space at its end.  The same will happen if the function<br />
returns the nil value.  If that function returns the empty<br />
string, the type will be ignored, but the blank lines or white<br />
spaces will be kept.<br />
<br />
In addition to element and object types, one function can be<br />
associated to the ‘template’ (or ‘inner-template’) symbol and<br />
another one to the ‘plain-text’ symbol.<br />
<br />
The former returns the final transcoded string, and can be used<br />
to add a preamble and a postamble to document’s body.  It must<br />
accept two arguments: the transcoded string and the property list<br />
containing export options.  A function associated to ‘template’<br />
will not be applied if export has option "body-only".<br />
A function associated to ‘inner-template’ is always applied.<br />
<br />
The latter, when defined, is to be called on every text not<br />
recognized as an element or an object.  It must accept two<br />
arguments: the text string and the information channel.  It is an<br />
appropriate place to protect special chars relative to the<br />
backend.<br />
<br />
BODY can start with pre-defined keyword arguments.  The following<br />
keywords are understood:<br />
<br />
&nbsp;&nbsp;:filters-alist<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;Alist between filters and function, or list of functions,<br />
&nbsp;&nbsp;&nbsp;&nbsp;specific to the backend.  See org-export-filters-alist for<br />
&nbsp;&nbsp;&nbsp;&nbsp;a list of all allowed filters.  Filters defined here<br />
&nbsp;&nbsp;&nbsp;&nbsp;shouldn’t make a backend test, as it may prevent backends<br />
&nbsp;&nbsp;&nbsp;&nbsp;derived from this one to behave properly.<br />
<br />
&nbsp;&nbsp;:menu-entry<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;Menu entry for the export dispatcher.  It should be a list<br />
&nbsp;&nbsp;&nbsp;&nbsp;like:<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(KEY DESCRIPTION-OR-ORDINAL ACTION-OR-MENU)<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;where :<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KEY is a free character selecting the backend.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DESCRIPTION-OR-ORDINAL is either a string or a number.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If it is a string, is will be used to name the backend in<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;its menu entry.  If it is a number, the following menu will<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;be displayed as a sub-menu of the backend with the same<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KEY.  Also, the number will be used to determine in which<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order such sub-menus will appear (lowest first).<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ACTION-OR-MENU is either a function or an alist.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If it is an action, it will be called with four<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;arguments (booleans): ASYNC, SUBTREEP, VISIBLE-ONLY and<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BODY-ONLY.  See org-export-as for further explanations on<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;some of them.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If it is an alist, associations should follow the<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pattern:<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(KEY DESCRIPTION ACTION)<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;where KEY, DESCRIPTION and ACTION are described above.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;Valid values include:<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(?m "My Special Backend" my-special-export-function)<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;or<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(?l "Export to LaTeX"<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;((?p "As PDF file" org-latex-export-to-pdf)<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(?o "As PDF file and open"<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(lambda (a s v b)<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(if a (org-latex-export-to-pdf t s v b)<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(org-open-file<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(org-latex-export-to-pdf nil s v b)))))))<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;or the following, which will be added to the previous<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sub-menu,<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(?l 1<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;((?B "As TEX buffer (Beamer)" org-beamer-export-as-latex)<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(?P "As PDF file (Beamer)" org-beamer-export-to-pdf)))<br />
<br />
&nbsp;&nbsp;:options-alist<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;Alist between backend specific properties introduced in<br />
&nbsp;&nbsp;&nbsp;&nbsp;communication channel and how their value are acquired.  See<br />
&nbsp;&nbsp;&nbsp;&nbsp;org-export-options-alist for more information about<br />
&nbsp;&nbsp;&nbsp;&nbsp;structure of the values.<br />

</div>
</div>
</details>

You have a few options.


### Customize the `org-export` LaTeX backend {#customize-the-org-export-latex-backend}

File properties


#### Backend {#backend}

There is `org-export-define-backend` and `org-export-define-derived-backend`. Defining a backend…

The latter is identical, only in that it inherits the definition of a parent backend. For instance, `beamer` is a derived backend from latex. I won't go in depth into this because you can read the docstrings for them.


#### Template {#template}


### Final title command {#final-title-command}


## Citations and bibliographies: Org-cite {#citations-and-bibliographies-org-cite}

Nowadays, with the comparatively new `org-cite`, citations and bibliography is the easiest step of this process. Readers should refer to [Citations in org-mode: Org-cite and Citar]({{< relref "citations-in-org-mode-org-cite-and-citar/index.md" >}}) for more information, but once `org-cite` is set up, potentially with `citar`, org-export will seamlessly export citations and bibliographies.

For reference, the following is my `org-cite` configuration (this doesn't include `citar`):

<details>
<summary><code>org-cite</code> configuration</summary>
<div class="details">

```emacs-lisp
(use-package oc
  :elpaca nil                           ; I use the `elpaca' package manager
  :general (:keymaps 'org-mode-map [remap citar-insert-citation] 'org-cite-insert)
  :custom
  (org-cite-global-bibliography kb/bib-files)
  (org-cite-csl-locales-dir (file-name-concat user-emacs-directory "locales/"))
  (org-cite-csl-styles-dir (expand-file-name "~/Zotero/styles/"))
  (org-cite-export-processors
   '((md . (csl "chicago-fullnote-bibliography.csl"))   ; Footnote reliant
     (latex biblatex)                                   ; For humanities
     (odt . (csl "chicago-fullnote-bibliography.csl"))  ; Footnote reliant
     (docx . (csl "chicago-fullnote-bibliography.csl")) ; Footnote reliant
     (t . (csl "modern-language-association.csl"))))    ; Fallback
  :custom-face
  ;; Have citation link faces look closer to as they were for `org-ref'
  (org-cite ((t (:foreground "DarkSeaGreen4"))))
  (org-cite-key ((t (:foreground "forest green" :slant italic))))
  :config
  ;; NOTE 2023-07-14: Require all `oc-*' packages so that I don't run into the
  ;; issue where the package associated with a style (e.g. `oc-biblatex' for the
  ;; biblatex style) in `org-cite-export-processors' is used prior to its
  ;; loading
  (require 'oc-natbib)
  (require 'oc-csl)
  (require 'oc-basic)
  (require 'oc-bibtex)
  (require 'oc-biblatex))
```
</div>
</details>


### One way to specify citation style {#one-way-to-specify-citation-style}

The value of `org-cite-export-processors` is most notable. The configuration above instructs org-export to use the `biblatex` export processor to process citations and bibliographies when exporting to LaTeX. This can be overridden, for example, at the file-level with the `#CITE_EXPORT` keyword. See the docstring for `org-cite-export-processors` for more information about valid values.

Notably, for LaTeX exports, **we can specify the style (citation style and bibliography style) of citations in this way.** For instance, taking a look at the commentary for the `oc-biblatex` library, we read:

<div class="verse">

;; In any case, the library will override style-related options with those<br />
;; specified with the citation processor, in \`org-cite-export-processors' or<br />
;; "cite_export" keyword.  If you need to use different styles for bibliography<br />
;; and citations, you can separate them with "bibstyle/citestyle" syntax.  E.g.,<br />
;;<br />
;;   #+cite_export: biblatex authortitle/authortitle-ibid<br />

</div>

This means that, for instance,

<style>.org-center { margin-left: auto; margin-right: auto; text-align: center; }</style>

<div class="org-center">

`#+cite_export: biblatex mla-new/authoryear`

</div>

results in `"bibstyle=mla-new,citestyle=authoryear"` being passed as an option to biblatex. Or we can set this in `org-cite-export-processors`:

```emacs-lisp
(setq org-cite-export-processors '((latex . (biblatex "mla-new/authoryear"))))
```

Valid styles include those provided by the built-in with biblatex, found in section 3.3.2 of the biblatex CTAN manual (link tk), and those provided by numerous other packages.


### My method to specify citation style {#my-method-to-specify-citation-style}

Though the above works, since I'm already using a .sty file to set all the options for the relevant LaTeX packages, I also do so there. For instance, with the MLA citation style:
In this way, I avoid adding a line in my org files to specify the citation style:

<style>.org-center { margin-left: auto; margin-right: auto; text-align: center; }</style>

<div class="org-center">

`#+cite_export: biblatex mla-new`

</div>


## Final result and screenshots {#final-result-and-screenshots}

The following org file produces this:

which, compiled into a PDF, looks like this:


## Extras {#extras}


### Section numbers {#section-numbers}

```elisp
(setq org-export-with-section-numbers nil)
```


### Hyperref {#hyperref}

```emacs-lisp
(setq org-latex-hyperref-template
      "\\hypersetup{
pdfauthor={%a},
pdftitle={%t},
pdfkeywords={%k},
pdfsubject={%d},
pdfcreator={%c},
pdflang={%L}
colorlinks={true},
hidelinks={true}}\n")
```


### Table of contents {#table-of-contents}

```emacs-lisp
(setq org-latex-toc-command
      "\\renewcommand{\\contentsname}{
  \\begin{center}
    Table of Contents
  \\end{center}
}
\\tableofcontents
\\newpage")
```


### Org links {#org-links}

1.  Custom `org-mode` links (`org-link-set-parameters`)


## Full configuration {#full-configuration}

(Make a note about that there are other stuff in the configuration not referenced or described tk.)

[^fn:1]: I use the term “title page” to reference what a stylesheet specifies as the format of a title, even if it does not create an entirely new page, e.g., MLA (rephrase tk).
