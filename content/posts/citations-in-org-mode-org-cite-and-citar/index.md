+++
title = "Citations in org-mode: Org-cite and Citar"
author = ["Kristoffer Balintona"]
publishDate = 2022-06-14T18:52:00-05:00
lastmod = 2023-11-20T23:59:00-06:00
tags = ["Emacs"]
categories = ["Guides"]
draft = false
series = "Writing papers in org-mode"
TableOfContents = true
+++

{{< alert >}}
<span class="timestamp-wrapper"><span class="timestamp">July 20, 2023</span></span> Readers should be aware that some package-specific information in this article may be outdated. Thus, readers are advised from outright copying code in this article without verifying (e.g. reading docstrings) the intended behavior. Nevertheless, the concepts related to `org-cite` and citation management should remain relevant and insightful.
{{< /alert >}}


## Summary {#summary}

First, I'll outline the broadest components to producing files in `org-mode` with a citation system (e.g. `org-cite`, `org-ref`). Then I'll introduce `org-cite` and the differences between its `bibtex`, `csl`, and `basic` export processors. Then, I will walk through my configuration for `org-cite` and `citar` (formerly `bibtex-actions`). You can see a [gist/snippet of all the code present in this post here (alongside other additions of mine)](https://gitlab.com/PreciousPudding/main-emacs/-/snippets/2351789).

My goal is to plot the landscape, preparing you to understand other available sources which are more suited for a user already familiar with the landscape.


## Helpful sources {#helpful-sources}

I recommend the following reading for more information regarding how `org-cite` works:

[Citation handling (The Org Manual)](https://orgmode.org/manual/Citation-handling.html#Citation-handling)
: The first source you should visit should almost always be the manual (available within Emacs via `info`, i.e. `M-x info`). It covers the fundamentals to understanding how to use `org-cite`. Isn't well documented currently, but that will change in the future.

[Org-cite guide by tecosaur](https://blog.tecosaur.com/tmio/2021-07-31-citations.html)
: I'll be referencing this frequently, for good reason.


## What are citation systems? {#what-are-citation-systems}

There are roughly three moving parts to citation management.

1.  **The tool that produces the bibliography file.**<br />
    A bibliography file is where information regarding sources are stored. This almost always comes in the form of a [.bib file](https://file.org/extension/bib); usually one is sufficient. There are various ways to create these files. Though you can manually create your own `.bib` files, the most common way is exporting a Zotero library (see [Zotero](#zotero)).
2.  **The text processor.**<br />
    This is the software the user (you) directly writes in. In our case, it's Emacs!
3.  **The “layer” from text to product.**<br />
    This is the code that lies in between your plain text writing and an output file (e.g. PDF). Sometimes this is identical to the text processor—in our case it isn't. For instance, Microsoft Word is both a text editor and the producer of an output file, such as a PDF or a `.docx`.

`org-cite` lies closer to (3) because it is responsible for exporting citations, whereas `citar` is closer to (2) because it is an interface for inserting citations into the buffer. A software like Zotero is responsible for (1).


## Org-cite {#org-cite}

In around July of 2021, `org-cite` was publicly released. `org-cite` a library packaged with `org-mode` that processes `org` citations (e.g. `[cite:@wittgensteinTractatusLogicoPhilosophicus2021]`). To those not familiar with another citation workflow (e.g. `biblatex` in LaTeX), it is quite daunting to leap into citations in Emacs. This was certainly the case for me: I leapt into `org-mode` citation integration with Zotero and LaTeX with no experience in `org`, Zotero, or LaTeX! Nonetheless, `org-cite` is quite simple once laid out. After reading this post or section, I recommend taking a look at tecosaur's [fabulous guide](https://blog.tecosaur.com/tmio/2021-07-31-citations.html) to using `org-cite`. His explanation of [the citation styles of org-cite and their variants](https://blog.tecosaur.com/tmio/2021-07-31-citations.html#cite-syntax) is particularly useful.


### Understanding `org-cite` export processors {#understanding-org-cite-export-processors}

The most crucial concept to `org-cite` are “export processors.” Take a look at the following passage from the documentation for `org-cite-export-processors` (as it is at the time of writing):

<div class="verse">

When non-nil, the value is an association list between export back-ends and<br />
citation export processors:<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;(BACK-END . PROCESSOR)<br />
<br />
where BACK-END is the name of an export back-end or t, and PROCESSOR is a<br />
triplet following the pattern<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;(NAME BIBLIOGRAPHY-STYLE CITATION-STYLE)<br />
<br />
There, NAME is the name of a registered citation processor providing export<br />
functionality, as a symbol.  BIBLIOGRAPHY-STYLE (respectively CITATION-STYLE)<br />
is the desired default style to use when printing a bibliography (respectively<br />
exporting a citation), as a string or nil.  Both BIBLIOGRAPHY-STYLE and<br />
CITATION-STYLE are optional.  NAME is mandatory.<br />

</div>

What this suggests is that different combinations of “processors,” “bibliography styles,” and “citation styles” can be configured to work with different `org-export` backends. Presently, there are three processors available: `basic`, `csl`, `natbib`, and `bibtex`.  Though `org-cite` citations will always follow the same format, **the output text that is exported will change depending on the processor used**. I will go over these differences now, focusing primarily on the `bibtex` and `csl` backends[^fn:1].

**The `bibtex` processor can only be used with the `latex` backend.** Using this processor exports citations using the [BibLaTex LaTeX package](https://www.ctan.org/pkg/biblatex). That means that `[cite:@bradleyAppearanceRealityMetaphysical2011 82–5;]` will export into a LaTeX command like `\cite[82–5]{bradleyAppearanceRealityMetaphysical2011}`. Compiling the `org` exported LaTeX file will in turn produce a PDF whose citations are handled by `biblatex`. The end _citation style_ will be determined by the provided package options (e.g. `style=mla-new`). Read the [biblatex documentation](https://www.ctan.org/pkg/biblatex) for more information (namely, section 3.1.1 in the February 2, 2022 version of the manual).

The `natbib` processor unsurprisingly uses the `natbib` LaTeX package. `biblatex` is slowly growing to replace the older `natbib` package, though some users may circumstantially be limited to `natbib`​—you can read more about the differences between `natbib` and `biblatex` [here](https://tex.stackexchange.com/a/25702). I have not personally used the `natbib`, but I suspect its usage is similar if not identical to the `bibtex` processor.

The `csl` processor will use CSL to export citations. [Citation Style Language](https://en.wikipedia.org/wiki/Citation_Style_Language) (CSL) is a language used to instruct the formatting of citations and bibliographies; citation styles are provided by `.csl` files. **Most importantly, CSL is intended to be a universal standard for citation formatting.** Accordingly, `.csl` supports essentially every output format.

In sum:

{{< mermaid >}}
flowchart LR
A((Plain-text)) --- B{Org-export}
B ===&gt; C
B ===&gt;D
B ===&gt; E

subgraph CSL Processor
C{{HTML, MD, ODT, plain-text, etc.}}
D[LaTeX]
D -.-&gt;|Compile with citations as rendered text| F(PDF)
end

subgraph Bibtex and Natbib processor
E[LaTeX]
E -.-&gt;|Compile with citations as LaTeX commands|G(PDF)
end
{{< /mermaid >}}


### Which processor should I use? {#which-processor-should-i-use}

The most common confusion comes from realizing that CSL can also export to LaTeX. So, when exporting to LaTeX: _should you use the `csl` or `bibtex/natbib` processor?_ The difference lies in whether or not you want to rely on the `biblatex` package to manage exports.

**The important difference is that `csl` exports to _rendered text_**. Rendered text is text whose end formatting will match that of `org`'s, e.g. italic text in `org-mode` will become wrapped in, say, `\emph{}` in LaTeX. So, for instance, if I export `[cite:@bradleyAppearanceRealityMetaphysical2011 82–5;]` into a LaTeX file using the `csl` processor using a CSL file for the Modern Language Association (MLA) citation style, I will see the following in-text citation in the LaTeX file as: `(Bradley 82–85)`​—if anything needed to be bold, italicized, etc., then that would be wrapped in the appropriate LaTeX command. Contrast that with the LaTeX command in the paragraphs above.

So when it comes to exporting to LaTeX, _when wouldn't rendered text be enough_? If you want the most basic functionality, i.e. “Seeing in-text citations and bibliographies in my documents is enough,” then CSL will always cover you. However, if you want access to the more complex or nuanced facilities of `biblatex`, use the `bibtex` processor instead. `biblatex` use cases include (i) using obscure or uncommon field and entry types and (ii) wanting “automation” in the citation variant. By (ii) I mean that `biblatex` handles the variant whereas you must manually list the variant in the `org-cite` citation when using CSL (see the above link to tecosaur's guide).

I will give an example. Basic MLA in-text citations following a `(AUTHOR PAGE)` format. However, closely following MLA means that subsequent in-text citations from the same source should have a `(PAGE)` format. Using the `csl` processor, without specifying a style or variant in the `org-cite` citation, will _always_ output the former formatting as rendered text; as [r/bdarcus explains in his comment](https://www.reddit.com/r/orgmode/comments/vchefn/comment/icgt0ql/?utm_source=share&utm_medium=web2x&context=3), CSL focuses on higher-level commands in order to be agnostic of output format. On the other hand, `org-cite` will, by default, leverage the `\autocite` command (see `org-cite-biblatex-styles`). This tells `biblatex` to **change the variant when appropriate**. This is an immensely powerful upside to using the `bibtex` processor—though it is minor in this particular case, this demonstrates the advantages of the `biblatex` package. You can read more about the differences between CSL and `biblatex` in this [thorough StackExchange explanation](https://tex.stackexchange.com/a/435004). Of course, however, `biblatex` is only available when exporting to LaTeX, whereas **CSL can be used to export into any format** (e.g. HTML, Markdown, ODT, plain-text).

_In sum_, use the `bibtex` processor **when you are exporting to LaTeX and only LaTeX** and

-   want automatic selection of citation variants
-   or have rare field or entry types in your `.bib` file.

_Otherwise_, use the `csl` processor[^fn:2] (assuming you have a `.csl` file for your desired citation style).

Finally, it should be noted that **the `csl` and `bibtex` processors use different locators in citations**. As explained in [tecosaur's guide](https://blog.tecosaur.com/tmio/2021-07-31-citations.html#cite-syntax), the `csl` processor uses locators like `bk.` and `vol.`, so citations may look like `[cite/noauthor:See@mollisonNietzscheContraStoicism2018 pp. 28–9;@priestWhyItIrrational2001 pp. 14;]`​—CSL processes these locators. On the other hand, a citation key's suffix is _directly passed as an option to BibLaTeX commands_, so an equivalent to the above would be `[cite/noauthor:See@mollisonNietzscheContraStoicism2018 28–9;@priestWhyItIrrational2001 14;]`, which doesn't have CSL locators.


### Configuration {#configuration}

Luckily, the difficulty in `org-cite` is only conceptually: configuring it is incredibly straightforward. There are two main variables that affect functionality. The first is crucial, and the second is useful to be aware of:

1.  `org-cite-global-bibliography`
2.  `org-cite-export-processors`

`org-cite-global-bibliography` is the bare minimum to getting `org-cite` ready to work. This should be a list of bibliography file (`.bib`) paths. I have mine set to a list whose single element is a file that Zotero exports:

```emacs-lisp
:custom
;; Actually, I set this value to `kb/bib-files', which is a list of file paths.
;; I set that as the value of any variable that requires bibliography files,
;; such as `citar', as you'll see below.
(org-cite-global-bibliography
 '("/home/krisbalintona/Documents/org-database/roam/references/master-lib.bib"))
```

Alternatively, you can use the org-mode `#+BIBLIOGRAPHY:` keyword to set this value per-file; it's value should be a file path.

`org-cite-export-processors` is useful as a means to tweak how you'd like `org-cite` to export citations. You can also set a file-local value using the `#+CITE_EXPORT` file keyword (e.g. `#+CITE_EXPORT: csl chicago-author-date.csl`); see [this section in tecosaur's guide](https://blog.tecosaur.com/tmio/2021-07-31-citations.html#more-exporting) for an example of how to set the processor, bibliography style, and citation style with this keyword. For a given backend[^fn:3], you can choose how `org-cite` exports it. For instance, here is mine:

```emacs-lisp
:custom
(org-cite-export-processors
 '((md . (csl "chicago-fullnote-bibliography.csl"))   ; Footnote reliant
   (latex biblatex)                                   ; For humanities
   (odt . (csl "chicago-fullnote-bibliography.csl"))  ; Footnote reliant
   (t . (csl "modern-language-association.csl"))))      ; Fallback
```

As you can see, I have CSL be the default in every case aside from LaTeX. The styles can either be a file path or a file name in `org-cite-csl-styles-dir`​—see [Zotero](#zotero) for how to easily acquire CSL files.[^fn:4] (I use LaTeX strictly for my academic papers, so I really enjoy `biblatex`'s features.)

I also choose to customize faces—I prefer the green faces that `org-ref` uses:[^fn:5]

```emacs-lisp
:custom-face
;; Have citation link faces look closer to as they were for `org-ref'
(org-cite ((t (:foreground "DarkSeaGreen4"))))
(org-cite-key ((t (:foreground "forest green" :slant italic))))
```


### Usage {#usage}

Using `org-cite` is incredibly simple:

1.  `org-cite-insert` to insert citations in `org` files, and
2.  `#+PRINT_BIBLIOGRAPHY:` within a document to mark where the bibliography/references/works cited should be.[^fn:6]


## Citar {#citar}

Using `org-cite` is really simple. But many users will find the interface… too bare. [Citar](https://github.com/emacs-citar/citar) is, roughly, the foremost attempt to provide a pleasing frontend to `org-cite`. It adds the `citar` citation-insert processor. The following is the **minimum** to start using `citar`:

```emacs-lisp
(use-package citar
  ;; The `:straight' keyword is not necessary. However, I do this to set a value
  ;; for the `:includes' keyword. This keyword tells use-package that those
  ;; package(s) are provided by this package, and not to search for them on
  ;; Melpa for download. Alternatively, you can set the `:straight' keyword to
  ;; nil in those package(s) use-package declaration.
  :straight (citar :type git :host github :repo "emacs-citar/citar" :includes (citar-org))
  :custom
  ;; A list of bib files. A good idea would be having its value be identical to
  ;; that of `org-cite-global-bibliography'. For me, I have all my bib file(s)
  ;; as a list of strings in `kb/bib-files'.
  (citar-bibliography kb/bib-files))

;; Use `citar' with `org-cite'
(use-package citar-org
  :after oc
  :custom
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar))
```

You can then set keybinds for common `citar` commands. For example, in the `:general` use-package keyword:[^fn:7]

```emacs-lisp
:general
(:keymaps 'org-mode-map
 :prefix "C-c b"
 "b" '(citar-insert-citation :wk "Insert citation")
 "r" '(citar-insert-reference :wk "Insert reference")
 "o" '(citar-open-notes :wk "Open note"))
```

If you use `embark` and `org-roam` you can also set these variables:

```emacs-lisp
:custom
(citar-notes-paths (list kb/roam-dir)) ; List of directories for reference nodes
(citar-open-note-function 'orb-citar-edit-note) ; Open notes in `org-roam'
(citar-at-point-function 'embark-act)           ; Use `embark'
```


### Capturing (Outdated) {#capturing--outdated}

{{< alert >}}
<span class="timestamp-wrapper"><span class="timestamp">August 19, 2023</span></span> The [citar-org-roam](https://github.com/emacs-citar/citar-org-roam) package provides all that is mentioned in this section and more. That pacage should be used instead in any instance of a desired integration between `org-roam` and `citar`.
{{< /alert >}}

If you use `org-roam`, then the following is how you select bibliographic source and create an `org-roam` node from it. All credit goes to [Jethro Kuan](https://www.jethro.dev/), the creator of `org-roam`, though I've made a few of my own customizations:[^fn:8]

```emacs-lisp
;; Create a new node from a bibliographic source. taken from
;; https://jethrokuan.github.io/org-roam-guide/
(defun kb/org-roam-node-from-cite (keys-entries)
  (interactive (list (citar-select-ref :multiple nil :rebuild-cache t)))
  (let ((title (citar--format-entry-no-widths (cdr keys-entries)
                                              "${author editor}${date urldate} :: ${title}")))
    (org-roam-capture- :templates
                       '(("r" "reference" plain
                          "%?"
                          :if-new (file+head "references/${citekey}.org"
                                             ":properties:
:roam_refs: [cite:@${citekey}]
:end:
#+title: ${title}
#+filetags: %(kb/insert-lit-category)\n")
                          :immediate-finish t
                          :unnarrowed t))
                       :info (list :citekey (car keys-entries))
                       :node (org-roam-node-create :title title)
                       :props '(:finalize find-file))))
```

I add it to an accessible keybinding:[^fn:9]

```emacs-lisp
:general
(kb/note-keys                           ; global-map
  "C" '(kb/org-roam-node-from-cite :wk "Citar-capture"))
```


### Advising `citar-org-update-pre-suffix` (Outdated) {#advising-citar-org-update-pre-suffix--outdated}

{{< alert >}}
<span class="timestamp-wrapper"><span class="timestamp">July 20, 2023</span></span> [Upstream has recently merged](https://github.com/emacs-citar/citar/commit/648ef552283fd9de18e6ccdf088d55fa26fe3cd8) changes to `citar-org-update-pre-suffix`, renaming it to `citar-org-update-prefix-suffix` and ensuring that a single space is left between citekeys and their suffixes. Therefore, the code directly below is now unnecessary.
{{< /alert >}}

A useful command is `citar-org-update-pre-suffix`. This command sets the prefix and suffix of a citation key (a single citation can have multiple keys). This is notable since a citation's suffix is where you provide a locator (e.g. a page number). The command is bound to `M-p` when the point is on a citation key.

Before advising `citar-org-update-pre-suffix`, I first advise `org-cite-insert` such that `citar-org-update-pre-suffix` is run immediately after inserting a citation:

```emacs-lisp
;; Run `citar-org-update-pre-suffix' after inserting a citation to immediately
;; set its prefix and suffix
(advice-add 'org-cite-insert :after #'(lambda (args)
                                        (save-excursion
                                          (left-char) ; First move point inside citation
                                          (citar-org-update-pre-suffix))))
```

A limitation with this code is that this only calls `citar-org-update-pre-suffix` on the last citation key, which is fine when only one key is inserted at once, and not when multiple are inserted. (Though, `citar-org-update-pre-suffix` can still be called manually on the other citation keys afterward.)

Additionally, I override `citar-org-update-pre-suffix` with my own version which adds the following functionality:

1.  Temporarily enable [typo-mode](https://github.com/jorgenschaefer/typoel).[^fn:10]
2.  Avoid manually inserting a separating space in the suffix.

<!--listend-->

```emacs-lisp
(defun kb/citar-org-update-pre-suffix ()
  "Change the pre/suffix text of the reference at point.

My version also adds a space in the suffix so I don't always have
to manually add one myself."
  (interactive)

  ;; Enable `typo' typographic character cycling in minibuffer. Particularly
  ;; useful in adding en- and em-dashes in citation suffixes (e.g. for page
  ;; ranges)
  (when (featurep 'typo)
    (add-hook 'minibuffer-mode-hook 'typo-mode)) ; Enable dashes

  (let* ((datum (org-element-context))
         (datum-type (org-element-type datum))
         (ref (if (eq datum-type 'citation-reference) datum
                (error "Not on a citation reference")))
         (key (org-element-property :key ref))
         ;; TODO handle space delimiter elegantly.
         (pre (read-string "Prefix text: " (org-element-property :prefix ref)))
         (post (read-string "Suffix text: " (org-element-property :suffix ref))))

    ;; Change post to automatically have one space prior to any user-inputted
    ;; suffix
    (setq post
          (if (string= (replace-regexp-in-string "\s-*" "" post) "")
              ""         ; If there is nothing of substance (e.g. an empty string)
            (replace-regexp-in-string "^[\s-]*" " " post))) ; Only begin with one space

    (setf (buffer-substring (org-element-property :begin ref)
                            (org-element-property :end ref))
          (org-element-interpret-data
           `(citation-reference
             (:key ,key :prefix ,pre :suffix ,post)))))

  ;; Remove hook if it was added earlier
  (remove-hook 'minibuffer-mode-hook 'typo-mode))
(advice-add 'citar-org-update-pre-suffix :override #'kb/citar-org-update-pre-suffix)
```

One can also add this command to `citar-citation-map`.[^fn:11]


### Aesthetics {#aesthetics}

Finally, we can change the way the `citar` interface appears. The most relevant variables are `citar-templates` and `citar-symbols`. Below is my configuration for these variables. Notably, my definition for `citar-symbols` utilizes `all-the-icons` icons to fancify the candidates (`all-the-icons` must be installed in order to have the faces below available):

```emacs-lisp
:custom
(citar-templates
 '((main . "${author editor:30}   ${date year issued:4}    ${title:110}")
   (suffix . "     ${=type=:20}    ${tags keywords keywords:*}")
   (preview . "${author editor} (${year issued date}) ${title}, ${journal journaltitle publisher container-title collection-title}.\n")
   (note . "#+title: Notes on ${author editor}, ${title}") ; For new notes
   ))
;; Configuring all-the-icons. From
;; https://github.com/bdarcus/citar#rich-ui
(citar-symbols
 `((file ,(all-the-icons-faicon "file-o" :face 'all-the-icons-green :v-adjust -0.1) .
         ,(all-the-icons-faicon "file-o" :face 'kb/citar-icon-dim :v-adjust -0.1) )
   (note ,(all-the-icons-material "speaker_notes" :face 'all-the-icons-blue :v-adjust -0.3) .
         ,(all-the-icons-material "speaker_notes" :face 'kb/citar-icon-dim :v-adjust -0.3))
   (link ,(all-the-icons-octicon "link" :face 'all-the-icons-orange :v-adjust 0.01) .
         ,(all-the-icons-octicon "link" :face 'kb/citar-icon-dim :v-adjust 0.01))))
(citar-symbol-separator "  ")
:init
;; Here we define a face to dim non 'active' icons, but preserve alignment.
;; Change to your own theme's background(s)
(defface kb/citar-icon-dim
  ;; Change these colors to match your theme. Using something like
  ;; `face-attribute' to get the value of a particular attribute of a face might
  ;; be more convenient.
  '((((background dark)) :foreground "#212428")
    (((background light)) :foreground "#f0f0f0"))
  "Face for having icons' color be identical to the theme
  background when \"not shown\".")
```

Here's the end-result:

{{< figure src="2022-06-15_12-52-26_screenshot.png" caption="<span class=\"figure-number\">Figure 1: </span>Visual improvements to `citar`'s minibuffer interface." >}}


## Other interesting packages {#other-interesting-packages}


### Zotero {#zotero}

[Zotero](https://www.zotero.org/support/) isn't an Emacs package, but it is crucial to most users' workflow, so it deserves attention. Zotero makes gathering, storing, and exporting bibliographic data infinitely easier. For instance, an ISBN or DOI of a source is all that is necessary for an entry's fields to be populated. There are two things to note about my setup:

1.  I install the [BetterBibLaTex](https://retorque.re/zotero-better-bibtex/installation/) add-on (see its features in the [official documentation page](https://retorque.re/zotero-better-bibtex/#features). Its main benefits include unique citation key generation and exporting facilities),
2.  and export my library using the BetterBibLaTeX format. I also tick the “Keep Updated” option; this continually updates the `.bib` file as I make changes to my library. I then point `org-cite-global-bibliography` and `citar-bibliography` to this file.

Additionally, for over a year now, I've been using Zotero's own PDF viewer and annotator. [Until recently](https://www.zotero.org/blog/zotero-6/), this feature had only been available in [Zotero Beta](https://www.zotero.org/support/dev_builds), but it is now merged onto the main branch. Though it isn't powerful, it gets the job done for me, and the convenience is what sells it.

Finally, a _very handy_ tip is to **use the CSL files that Zotero already provides** (credit to [this section of tecosaur's guide](https://blog.tecosaur.com/tmio/2021-07-31-citations.html#working-with-zotero)). The CSL files (which provide citation styles) are located in `ZOTERO_DIR/styles/`. You can set `org-cite-csl-styles-dir` to this path:

```emacs-lisp
:custom
(org-cite-csl-styles-dir
 (expand-file-name "~/Documents/Zotero/styles/"))
```


### Embark {#embark}

[Embark](https://github.com/oantolin/embark/) provides menus (essentially keymaps) based on “context,” that is, the type of thing the point is on. As a result, `embark` has immense potential. `Citar` is built to exploit `embark` without requiring it—for instance, see `citar-citation-map`.


### Org-roam-bibtex {#org-roam-bibtex}

[Org-roam-bibtex](https://github.com/org-roam/org-roam-bibtex) integrates [org-roam](https://github.com/org-roam/org-roam) with citation systems, such as `org-cite`. This packages allows `org-roam` to recognize particular nodes as bibliographic (i.e. associated with a particular `.bib` entry).

Additionally, as [as `citar`'s author, u/bdarcus, shares here](https://www.reddit.com/r/orgmode/comments/vchefn/comment/icg0sha/?utm_source=share&utm_medium=web2x&context=3), there is work being done on `citar`'s end to more tightly integrate `org-roam` with `citar`, in the form of `citar-org-roam`.


### Citar-capf {#citar-capf}

[Citar-capf](https://github.com/mclear-tools/citar-capf) provides a `complation-at-point-function`[^fn:12] for citations. However, if you're reading this in the future, there's a likely chance that [this commit](https://github.com/emacs-citar/citar/commit/784791a090f9a2ed293f8b3ebd5636c690b718bc) would have been merged. The commit merges `citar-capf` with `citar`.


### Org-ref {#org-ref}

[Org-ref](https://github.com/jkitchin/org-ref) has historically been the dominant citation system for `org-mode`. I elect to use `org-cite` instead because (i) it comes with `org-mode`, meaning there is one less conceptual- and configuration-layer to navigate, (ii) Moreover, `org-ref` uses a format that follows `org` conventions less closely, and (iii) I have read that is arguably more robust and future-proof. Nevertheless, some users still prefer it.


## Changelog {#changelog}

-   <span class="timestamp-wrapper"><span class="timestamp">June 15, 2022</span></span>
    -   Better clarity in [Advising `citar-org-update-pre-suffix` (Outdated)](#advising-citar-org-update-pre-suffix--outdated).
    -   Mention the merging of `citar-capf` into `citar` (see [Citar-capf](#citar-capf)).
    -   Added [Embark](#embark).
    -   Mention `citar-org-roam` in [Org-roam-bibtex](#org-roam-bibtex).
    -   Made comments clarifying `kb/bib-files`.
    -   Add screenshot that showcases visually improved `citar` interface (see [Aesthetics](#aesthetics)).
-   <span class="timestamp-wrapper"><span class="timestamp">June 19, 2022</span></span>
    -   Added [Helpful sources](#helpful-sources).
    -   Change processor explanations to include the `natbib` processor.
    -   Change processor explanations to recognize the difference between plain-text and rendered text.
-   <span class="timestamp-wrapper"><span class="timestamp">July 20, 2023</span></span>
    -   Noted that some package-specific information in this article may be outdated.
    -   Noted update to `citar-org-update-pre-suffix`
-   <span class="timestamp-wrapper"><span class="timestamp">August 19, 2023</span></span>
    -   Mentioned `citar-org-roam` as preferable to any desired integration between `org-roam` and `citar`.
-   <span class="timestamp-wrapper"><span class="timestamp">November 20, 2023</span></span>
    -   Updated GitLab Gist.

[^fn:1]: The `basic` processor provides bare functionality and should never be preferred over the alternative, if possible.
[^fn:2]: See [Configuration](#configuration).
[^fn:3]: This is an `org-export` backend. This means that, for instance, `md` refers to the `org-export` `md` backend as well as any other derived backends. Read the documentation for `org-export-define-derived-backend`.
[^fn:4]: Notice that a citation style is required for the `csl` processor, and not for the `bibtex` processor.
[^fn:5]: Be aware that I use `use-package`'s `:custom-face` keyword to redefine these two faces. One can just as easily use the built-in `defface` of `set-face-attribute`.
[^fn:6]: Also see [this section of tecosaur's guide](https://blog.tecosaur.com/tmio/2021-07-31-citations.html#basic-usage).
[^fn:7]: You can also just use the built-in `define-key`.
[^fn:8]: This assumes that you have `org-roam-bibtex` installed and enabled. See [Org-roam-bibtex](#org-roam-bibtex).
[^fn:9]: `kb/note-keys` is my own `general.el` leader key. I've mapped its prefix to `C-c n`.
[^fn:10]: I prefer directly inserting curly quotes, em-dashes, and en-dashes into my buffers. I enable `typo-mode` here so that typing page ranges properly inserts en-dashes.
[^fn:11]: See [Embark](#embark).
[^fn:12]: Also see [Vertico, Marginalia, All-the-icons-completion, and Orderless]({{< relref "vertico-marginalia-all-the-icons-completion-and-orderless/index.md" >}}).
