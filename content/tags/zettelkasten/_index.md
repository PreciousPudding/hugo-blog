+++
title = "Zettelkasten"
date = 2022-12-24T02:01:00-06:00
draft = false
+++

The Zettelkasten (German for “slip box”) was Niklas Luhmann's (1927–1998) note-taking method.[^fn:1] Luhmann was an accomplished and, importantly, prolific writer and researcher of sociology and philosophy of social science. Luhmann attributes his Zettelkasten to his accomplishments.

[^fn:1]: There have been many similar independently developed analogy note-taking methods in the past, some even dating back hundreds of years ago.
