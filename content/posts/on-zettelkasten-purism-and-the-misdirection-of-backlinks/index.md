+++
title = "On Zettelkasten purism and the misdirection of backlinks"
author = ["Kristoffer Balintona"]
publishDate = 2021-07-27T17:37:00-05:00
tags = ["Zettelkasten"]
categories = ["Essays"]
draft = false
aliases = ["/on-zettelkasten-purism-and-the-misdirection-of-backlinks/"]
+++

## TL;DR {#tl-dr}

Don't be distracted by the allure of backlinks; a good knowledge management practice provides more value. Luhmann's Zettelkasten isn't law; your needs should instruct the directives you follow.


## I disagree with ‘backlink exceptionalism’ {#i-disagree-with-backlink-exceptionalism}

The commonly understood novelty of the Zettelkasten tool is its non-hierarchical organization composed of hyperlinks. Backlinks generate interconnected networks of thought which mirror the associative nature of our brains. It is a departure from traditional hierarchical note-taking practices. Looking at the outlier of Niklas Luhmann, we can be quick to conclude that the backlink is the cause of Zettelkasten's efficacy. The novelty of backlinks has made the concept synonymous with ‘Zettelkasten.’ However, there are many successful personal knowledge management systems, and it would be unwise to study only a sample size of one, decontextualizing Luhmann's methodology from other successful knowledge management systems.

The spotlight is regularly on the serendipity and surprise from backlinks from an emergent network of interconnected notes. However, this is too narrow of a spotlight; there are many other changes in note-taking and production. For instance, the time spent writing — and thus thinking — is increased. There is a heightened intentionality from creating links, rearranging ideas, and concretely classifying new information. Deliberate writing and thinking has benefits to production which are too scarcely acknowledged. That is what I wish to address here.


## The ‘practice’ is most essential {#the-practice-is-most-essential}

At the broadest level, what makes a Zettelkasten effective? Firstly, a faithful Zettelkasten should help produce written pieces. Secondly, Zettelkasten's note-taking process facilitates idea mastery and emergence. The latter induces the former because the artifacts of that note-taking process are directly used in future works; much of the thinking is often done before the first draft is even begun. Take away those artifacts, and you're left with something awfully similar to a conventional note-taking system. Thus, we arrive at idea mastery as the core vehicle for Zettelkasten's success.

I ask: from what does most of your mastery over new material arise? How does Zettelkasten facilitate idea mastery and emergence? Most would turn to the feature of backlinks because of its perceived novelty. However, I counter the common sentiment by claiming that idea mastery mostly arises from the _practice_ associated with maintaining a Zettelkasten. I argue Zettelkasten's essence is to **repeatedly revisit, recall, and engage with that which you have learned, iterating upon the writing artifacts of that process — your crystallized thoughts — over time**. Engaging with what you learn is the crux to a successful knowledge management system. The contribution of backlinks is too commonly conflated with the contribution of that routine. Backlinks are a complementary feature.


### An aside: but what about backlinks?! {#an-aside-but-what-about-backlinks}

I will address a potential concern. Namely, to underappreciate the power of backlinks is to discard the essential power of associative thinking. However, we must remember that we naturally think associatively irrespective of what system we use. Luhmann's Zettelkasten is just the most formalized system (I know of) for this process. We need not be interdisciplinary to Luhmann's degree; serendipitous discovery is not necessary for interdisciplinary work. At the very least, we should settle for a ‘good enough’ system and be productive, rather than working to find the perfect system. Surely, devoting all your energy to creating the perfect system is putting the cart before the horse — good thinking and meaningful work take priority.


## Luhmann isn't the only success story {#luhmann-isn-t-the-only-success-story}

I observe similar principles intuitively arrived at by other incredibly productive and learned thinkers. It is common for scholars to reread books, have discourse with other intellectuals, obsess over ideas in gaps in their day, and, perhaps most importantly, write and talk about it. For instance, many authors and writers espouse the idea of a Commonplace Book, traditionally a journal which contains quotable passages, personal comments, and literary excerpts. Ryan Holiday, a prolific reader and author who has published over ten books in just a decade, is famously known for his own incarnation of the Commonplace Book known as The Notecard System. His system was directly adopted from one of his mentors, another bestselling author by the name of Robert Greene. I currently attend university and more than one of my professors have intuitively created their own knowledge management systems and found success in their academic work. One of those professors recommended I read _How to Write a Thesis_ by Umberto Eco, which I found to be a surprisingly close analog to Luhmann's Zettelkasten. The Zettelkasten community is also very familiar with Andy Matuschak's Evergreen Notes, which is inspired by, but significantly departs from, Luhmann's Zettelkasten. Also in this list are many notable figures in history including George S. Patton and Ronald Reagan. In short, Zettelkasten isn't the only successful knowledge management system.

All of these systems have found incredible success, yet are not Zettelkasten; their particulars drastically differ. How? I have found that the details of these systems — when a note is made, when it is returned to, how sources are managed, et cetera — differ widely but are united by the common directive to **continually linger with the material**.


## So what do you do now? {#so-what-do-you-do-now}

Methodologies _should not be dogmatic_. The Notecard System, Evergreen notes, the Commonplace Book… Luhman's Zettelkasten is just one incarnation of a personal knowledge management system out of many. The very fact that there are so many methodologies demonstrates that none are a panacea. Purism is counterproductive — it confines expression to Zettelkasten-esque formulations. Treat them as resources and conclude your own decisions. Eclectism is a sign of thoughtfulness and intentionality.

Furthermore, Luhmann likely did not begin with an awareness of the exact rules and principles his Zettelkasten would ultimately have. Rather, he began with an initial premise and built his Zettelkasten — and its rules — around what he needed: a means to think interdisciplinarily and broadly, enough to form a ‘Theory of Society.’ Thinking interdisciplinarily and broadly are attractive and most would no doubt value those benefits in a Zettelkasten system. _But should that be the North Star for your Zettelkasten_? Is that your ultimate goal in knowledge management? You must appreciate the difference between your goal and Luhmann's. If they do not align, your system will not serve you as well as it should. Worse, if you do not have a clearly defined and articulated purpose, then your Zettelkasten will become a hodgepodge of things gathered elsewhere, vying for your attention to use it this way and that. It will be undirected and reliant on luck to stay afloat.

Indeed, new Zettelkasten users often get swept away by the semantics of “What does it mean for a note to be atomic?” or “What should be in a literature note instead of a Zettel?” or “When should I make a note?”. On the other hand, most longstanding users of Zettelkasten intuitively arrive at the same conclusion as me: **consider what others have done, and take what is appropriate for you**.


## How has my own advice affected me? {#how-has-my-own-advice-affected-me}

Once I disabused myself from the notion that I should know exactly what Luhmann's principles were in order to follow his rules to a T, everything fell into place. Instead of thinking that a malfunction or failure of my system reflected my own lack of understanding, I started to adopt a problem-solving mentality, treating other implementations as resources and making my own judgments. For the first time my Zettelkasten started truly being effective.
