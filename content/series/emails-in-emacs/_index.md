+++
title = "Emails in Emacs"
date = 2023-01-03T02:58:00-06:00
draft = true
+++

In this series, I go over the ins-and-outs of emailing in Emacs. Namely, I go over how to set up Emacs to (i) retrieve and read emails (i.e. `mbsync` or `offlineimap` and `mu` or `notmuch`) and (ii) send emails (i.e. `message-mode`, `sendmail` or `smptmail`, and `org-msp`).
