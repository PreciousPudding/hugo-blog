+++
title = "Knowledge: A Very Short Introduction by Jennifer Nagel"
author = ["Kristoffer Balintona"]
date = 2023-01-02T23:14:00-06:00
publishDate = 2023-01-01T17:34:00-06:00
tags = ["Philosophy"]
categories = ["Book Reflections"]
draft = false
+++

[Knowledge: A Very Short Introduction by Jennifer Nagel (2014)](https://www.goodreads.com/book/show/20749121-knowledge)&nbsp;[^fn:1] is a phenomenal introduction to epistemology. Spanning a little over 100 pages, one would not expect an in-depth overview of the field. Indeed, Nagel does not bog down the reader with terms nor the intricacies epistemologists would be interested in---but the ideas often linger in the air for the reader to dwell on, and Nagel doesn’t fall short in teasing the magnitude of what hasn’t been explained. Unequivocal, the pace of the book is digestible yet the book demands the reader stay on their toes---each paragraph is productive.

Nagel is incredibly accessible and engaging. He does an outstanding job (i) using demonstrative examples accessible to the layperson, (ii) inserting examples that probe the inquiry, (iii) and treating each topic evenhandedly, not privileging one view over another (at least, from the perspective of a novice reader). Consequently, the reader feels naturally propelled from one inquiry to another. This is another strength of Nagel: the book is inquiry-driven, that is, grounded by the common sense questions that follow an exposition. He slithers through the touchstone topics of epistemology with ease: skepticism, rationalism and empiricism, internalism and externalism.

Of course, such brevity can only be achieved by omitting a lot of what would otherwise be interesting to the reader. Nagel doesn’t present even approximations of a “final answer;” there are only open questions here. But, the book fulfills the role readers would expect it to fill: it enticingly introduces subtopics within epistemology that invites curiosity.

[^fn:1]: The Oxford [A Very Short Introduction](https://global.oup.com/academic/content/series/v/very-short-introductions-vsi/?cc=us&lang=en&) series is a phenomenal set of introductory books. The series is highly regarded and spans dozens of fields and subfields. Each entry in the series generally maintains more-or-less a standard of brevity (~100 pages) and clarity.
