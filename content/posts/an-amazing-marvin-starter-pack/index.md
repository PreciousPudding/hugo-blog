+++
title = "An Amazing Marvin Starter Pack"
author = ["Kristoffer Balintona"]
publishDate = 2021-06-24T17:37:00-05:00
tags = ["Amazing Marvin"]
categories = ["Guides"]
draft = false
aliases = ["/posts/an-amazing-marvin-starter-pack/", "/an-amazing-marvin-starter-pack/"]
TableOfContents = true
+++

## TL;DR {#tl-dr}

**Read this if you are either** 1) interested in and want to try out Amazing Marvin, 2) just started using Amazing Marvin but feel overwhelmed, or 3) don't know what Amazing Marvin is but want to read about a few of its features.

**This post goes over** the peculiarities of Amazing Marvin — namely the _Master List_, _Main View_, and _Strategies_. Understanding these concepts is the first step to leveraging the strengths of Amazing Marvin to create your own workflow.

**I've also added** a list of the first Strategies a new Mavinaut (users of Amazing Marvin) should check out.

**By the end** you'll have a grasp of Amazing Marvin's foundational components and ready to start building your own workflow.


## Core concepts {#core-concepts}


### The Master List {#the-master-list}

{{< figure src="2022-05-29_02-08-02_screenshot.png" >}}

You open Marvin. You're greeted with a friendly UI — you notice buttons and a sidebar. Press “M” and you'll be taken to the **Master List**. This is the place where you can see _all_ your tasks. Think of it as the closest thing to a generic todo list application, like Microsoft Todo. You can view your _tasks_ and _projects_ (which can be further separated into _categories_) in either a column or list view (try it out!).

You may also notice a sidebar on the left which also lists tasks and projects — this can be toggled using “X”. This will be handy in the future, but isn't necessary to worry about for now.

In the Master List, I sometimes add tasks in bulk, which will often be when I want to flesh out tasks for a project.
Though you can easily access all your projects and tasks using the Master List, you will likely not spend much time here.


### The Main View {#the-main-view}

{{< figure src="2022-05-29_02-05-26_screenshot.png" >}}

The place where you will most interact with your todos and get things done is the **Main View**. The Main View is where _you_ decide which tasks are shown, when they are shown, and how they are shown to you by taking advantage of Amazing Marvin's customizability.

The Main View is centralized around being able to show you what you want to do today. This doesn't, however, necessarily mean that Marvin is limited to showing you tasks which are due today. Amazing Marvin is more powerful than that and has the flexibility to accommodate so many workflows.


## What good is Amazing Marvin's system? {#what-good-is-amazing-marvin-s-system}

Those are the two most important pieces to wrap your head around using Amazing Marvin. But why depart from the typical todo list format?

The strength of this system is that you have access to all your tasks in your Master List but don't have to look through these long task lists — even sorted or filtered, they are overwhelming and dissuasive to productivity — in order to get things done. The Main View creates a divide between working on your tasks and viewing them. This is a surprisingly powerful feature of Marvin, which removes much of the difficulty of trying to juggle tasks within folders or across tags. You choose when to see the subset of tasks you want to work on, and go to the Master List when you choose to see all of your tasks.


## The beauty of **Strategies** {#the-beauty-of-strategies}

On top of that, Marvin is still feature-rich yet manages to limit that complexity for an easier learning curve. How Marvin accomplishes this is through its clever **Strategies** system. Strategies allow you to only enable the features which are useful to you. They can be thought of as _togglable features_.

Strategies are one of the aspects that make Amazing Marvin shine. It gives you the flexibility to implement as many strategies as you want (again, think of them as features) into your workflow or be as minimalist as possible and use only a select few. In the end, Amazing Marvin _can be customized to how you think and work_.

This is different from other task management apps, which follows the “you-get-what-you-see model.” In other task management apps, users have to use a space in which every feature, whether useful to them or not, coexists. This is especially the case with bloated or other feature-rich task management apps. In this way, Marvin can be feature-rich but much less overwhelming than other task management solutions.

Amazing Marvin goes a step further by providing brief yet useful descriptions for every Strategy on how to use it. Additionally, there is often an accompanying short video (made by Christina, one of the founders, herself) demonstrating what the Strategy looks like in action. Finally, most strategies have a list of settings which allow you to further fine tune your experience.


## A list of must-look-at Strategies {#a-list-of-must-look-at-strategies}

To conclude, I've included several lists of Strategies that should be taken a look at as soon as possible as a new user. In addition to checking these Strategies out, **Workflow Templates** are curated sets of settings and strategies which have been pre-made for you. They range from small snippets to full workflows. I'd _highly recommend_ looking through these templates as a starting point for your workflow.

**Standalone, non-intrusive, must-use Strategies**

1.  Subtasks
2.  Mobile sidebar
3.  Check off Behavior
4.  Right-click menu
5.  Hover button actions

**Other common Strategies which are easy to set up**

1.  Quick add
2.  Task notes
3.  Category context
4.  Skinny sidebar
5.  Sidebar

**Strategies you will likely be looking for if you've used other task management applications**

1.  Labels (tags)
2.  Task reminders
3.  Duration estimates
4.  Full-calendar functionality
    1.  Calendar
    2.  Agenda
    3.  All-day items
    4.  Events
5.  Smart lists (?)
6.  Priority levels
7.  Sequential projects

**Useful Strategies which are standalone, easy to understand, yet aren't found in almost any other task management app (Experiment with them!)**

1.  Work session scheduler
2.  Eat that Frog
3.  Procrastination count
4.  Backburner
5.  Time tracking
6.  Missing Next Step warning
7.  Timers


## Author's Note {#author-s-note}

I have not been told to write about Amazing Marvin. I receive no compensation from Amazing Marvin for this post. I only want to share this amazing piece of software with others.
