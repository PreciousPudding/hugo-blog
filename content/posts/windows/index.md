+++
title = "Windows"
author = ["Kristoffer Balintona"]
publishDate = 2021-08-10T21:30:00-05:00
lastmod = 2022-03-05T04:30:00-06:00
tags = ["creativewriting"]
categories = ["Creative Writing"]
draft = false
aliases = ["/posts/windows/"]
+++

## Beauty {#beauty}

Rarely do moments of clarity arrive: ephemeral gifts recognized only a beat too late. As an exercise in free association, my memory draws, once again, to that thunderstorm.

It materialised slowly yet caused me little alarm, not unlike my relationship with my dear window. I think its gradual pace _is_ the reason why I didn't notice it. But what I did notice was a feeling. An intangible awareness. The winds shivered ever so slightly, a seemingly imperceptible turbulence in the air.

I find this feeling akin to a fun-fact I read years ago. Buried in a forum thread is a comment that reads something along these lines; I work as a paramedic. I have a lot of experience with these kinds of situations. From my experience, we have some sort of inherent sense that something is wrong. When a patient tells me they're going to die, or they have this intense fear in their eyes — not the normal kind, but a deep, infinite kind — something bad happens very soon. Something fatal like a heart attack, for instance, strikes minutes later. The human body just knows. I can't corroborate this fact or the anonymous tale, but its veracity is irrelevant — I entertain a faith in this phenomenon. Almost like a dog instinctively barking at a brewing tornado, I felt a compelled certainty.

Entranced in my chair, I watch the flash seep everywhere. It confirms my gut instinct. With it passed, and my brain rebooted, I recognize this as a familiar yet confusing scene: Shouldn't there... CRASH! The off-beat thunderclap shook me.

In this tiny space, I feel, for the first time, like I was living in more than just the room I call mine. The label of ‘room' became inappropriate. It was at this moment, from a mere open window, that I learned sensations could be so raw. Such an oceanic largeness on its other side; an immensity that demands humility. I wondered: Why have I just now noticed this?

---


## Discomfort {#discomfort}

The season: Summer. The temperature: Scorching. The consequences of that heat are especially urgent on my soles. Not the entire flat of my feet, just two spots: one where my first and second toes wrap around the wire of my flip-flops, and another near my heel where the wire inserts into the sole. These particular points dig into my skin at every step. Jutting from the landscape of my East Coast campus are spurts of hills and plateaus. Unfortunately for me, Google Maps apparently demands the pain of managing uneven terrain perfectly conducive to the pricking of soles. The twists and turns in these narrow, one-way streets don't help either.

“My god, why is Providence so damn hilly...” I can't help but feel disadvantaged for having been raised in Chicago, the land of ‘unchanging-altitude.'

That's the acute discomfort. Demanding my focus chronically is the humid stickiness that permeates every surface of my body. At this point, my clothing feels more like soggy paper. The household walls across the street and close to my right are high, variegated, and annoyingly bare. With no passerby in sight so far, there is no escape. I am alone in this mundane struggle. The only saving grace from the incessantly burning sun is the relief of my first in-person class. I'm not bubbly or giddy, just expectant mixed with a tinge of nervousness.

I welcome the sun's immense and uncomfortable pressure. It's too good of a coincidence that the heat advisory warning overlapped with this momentous occasion — already delayed by two weeks, in fact. I tend to entertain myself with my own humor nowadays. I think it's a habit I developed who-knows-when during that swath of solitude. All-in-all, I consider the sun's grace a harsh “welcome back.”

---


## Inquiry {#inquiry}

I admire it.

My window has an audience. A picture frame of a poem gifted as an off-to-college present from my mother; a duet of flasks, one tall and skinny, the other short but wide; a metal cup with a handle, perfect for tea and water; my ivy plant, whose leaves number more than seven times the initial five it started with when I brought it to Brown. Behind the main characters of the stage — the foreground you could say — is the unsuspecting setting. Unclear glass muffled from fingerprints and residue. A pure guess, I assume that the frame is wood coated with white paint. Contrasting the aged glass is this wine-like wood: age evident but not distasteful. A grid screen sits just behind it. It stops the bugs from getting in, and me from falling out.

Most of the time the pane is lifted more than a foot above its closed position. How wonderful such a simple change has been. Shallowly, this story is about the way my window has dyed the color of my first year of college: positively. Deeply, on the other hand, is a commentary on our sheltering, which twists sanctuary into captivity. What have we isolated ourselves from?

---


## Without {#without}

Someone I knew once said, “I started playing chess when I was five.”

“Oh, is that why you're so good now?”

“I'm not that good.”

“Your rating is literally 1800!”

In Freshman year of high school, I met someone who had been a gymnast since the third grade. One of my close friends had been playing piano since kindergarten. It's a usual occurrence for these outliers to broadcast themselves on YouTube or Instagram — a knack for art paired with an intractable sum of dedication. That isn't me.

My idols, none of which I've actually met, tend to have a childhood filled with _something_. I did not. Vacuous is how I'd describe myself. But this description is all retroactively applied. I say this now with the knowledge of a bigger world, filled with more stresses and joys alike. I picture my young environment as hollow because time didn't really exist. My memories of a time when urgency was an undefined sensation are fond: such a stark contrast to life now. That basement and even tinier living room was my world, my detention. Existence was what was immediately in front of me: the TV.

“Today on How It's Made, we'll learn about how erasers first...”

“The Kid's Next Door!...”

“But Finn, you can't...”

Although I reimagine myself as being silent and unnoticed, it was the other way around: the world around me was unnoticed. Unnatural. That infinitesimally small space was only so because I couldn't see something larger. I couldn't see more of the world — physically and metaphorically. There was much, much, much more beauty to behold. So much more chaos and serendipity. So much more to appreciate and wonder and stare at. When your world is the only one you know, you can't see anything but that.

---


## Comfort {#comfort}

DRRRING!!! DRRRRRING!!! DRRRRRRING!!!...

There are things that cannot be done when you are in a rush. Waking up is one of those things for me. Waking up is tormenting. My mind resists being rustled. Far too easily, I shove my late-night reminders behind the warming luxury of blankets.

In my struggle, a break in the clouds becomes apparent. Literally. I listened to a podcast a few months ago about how light rays, especially those that hit your eyes directly from the sun — those not refracted and scattered through a window's glass — are essential to the wake of the body. I keep my left eye a tenth open but the right completely shut. The left one can't even do that for much longer than a few moments before its accumulated nocturnal debris grows too troublesome — but it's enough for me to find the outline of a certain black rectangle. I need to shake it because I use a special alarm clock app. It's a preventative measure for a chronic over-sleeper. All that matters is that it's been doing its job. The fact that I'm conscious enough to have this thought proves my point.

I couldn't help but notice the unfettered rays peering through the opening. Stopped a foot above the windowsill is the bottom of my blinds. I'm reminded of my foresight last night to lower them so that my present self's retinas wouldn't be burned. I mentally pat myself on the back for it. I then laugh at myself for mentally giving myself a pat on the back.

At any rate, the sunlight demands my attention. It is bright but balanced by the darkness of the crevices it cannot reach on my messy table. The area is bright enough to stir yet dark enough to soothe. I'm surprised at how natural this feels — was it always like this? No, my old room didn't even have a window in the first place. In my trance, I realize the coherency of my thoughts. I rather quickly raise my upper half from under my tempting sheets, rub both eyes with either hand, and check the time.

---


## Unnoticed {#unnoticed}

If your second semester in college was unexceptional, then yours wasn't so far off from mine. Mostly monotonous weeks passed until any novelty arose at all. But only an inkling, a turning ambiance: an inappreciably small shift. I stand at a distance, across the room, far from the window. Peering through it produces in me a feeling I never knew I yearned for. Even as I type this paragraph several weeks later, I sense a radiating, motherly familiarity.

An inanimate object, this window reminds me of our fickle randomness. Unappreciated. Unmoved. Unnoticed. Our myopia dawns on me. We stumble through life, deceiving, loving, becoming learned, then sputter out within the span of a dozen tree rings. What possesses you? Is it your career? Your homework? Money? Has gasping for breath at the workday's close become routine? Reflect on your day-to-day: has a moment ever penetrated into _you_ as much as this window has into me?

---


## Author's Note {#author-s-note}

This is a piece I wrote for a creative non-fiction class — I didn't write it with a public audience in mind. Furthermore, this is the second piece of creative non-fiction I've ever produced. Take what you will from it.

-   <span class="timestamp-wrapper"><span class="timestamp">March 5, 2022</span></span> This piece has been somewhat edited and published separately by [Sole Magazine](https://solemagazinebrown.wordpress.com/2022/03/04/windows/).
