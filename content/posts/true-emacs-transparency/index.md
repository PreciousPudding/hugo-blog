+++
title = "True Emacs Transparency"
author = ["Kristoffer Balintona"]
publishDate = 2022-06-07T10:00:00-05:00
lastmod = 2022-06-08T00:00:00-05:00
tags = ["Emacs"]
draft = false
+++

Transparency in Emacs isn't that great. Every source I find regarding transparency of Emacs (as of writing this) points to setting the `alpha` frame parameter. For instance:

1.  [EmacsWiki: Transparent Emacs](https://www.emacswiki.org/emacs/TransparentEmacs)
2.  [How can transparency be toggled? - Emacs Stack Exchange](https://emacs.stackexchange.com/questions/22663/how-can-transparency-be-toggled)
3.  [How to set a key binding to make Emacs as transparent/opaque](https://stackoverflow.com/questions/2935520/how-to-set-a-key-binding-to-make-emacs-as-transparent-opaque-as-i-want)
4.  [Any Advice On Getting Transparency To Work In Emacs Window Manager](https://www.reddit.com/r/emacs/comments/ljev64/any_advice_on_getting_transparency_to_work_in/)

The issue with `alpha` is that it sets the transparency of the _entire window_; what this means is that _text and background_ become transparent together. Clearly this is undesirable for some: the `alpha` parameter needs to be near 100, otherwise the text becomes illegible. Ideally, the text remains opaque while the background becomes transparent.[^fn:1]

The solution came from a [patched version of Emacs made by Håkon Flatval](https://github.com/TheVaffel/emacs) back in November 2021—his announcement that month [on Reddit](https://www.reddit.com/r/emacs/comments/qm3tsv/i_implemented_partial_support_for_background/) received some attention. The patch adds the `alpha-background` frame parameter which makes **only the background** of a frame transparent:

{{< figure src="2022-06-05_20-57-01_screenshot.png" caption="<span class=\"figure-number\">Figure 1: </span>`alpha-background` set to 75." >}}

You can set the `alpha-background` of a particular frame with the `set-frame-parameter` function, and the default value for frames by adding to `default-frame-alist`.[^fn:2] For example:

```emacs-lisp
(set-frame-parameter nil 'alpha-background 100) ; For current frame
(add-to-list 'default-frame-alist '(alpha-background . 100)) ; For all new frames henceforth
```

I've also defined my own command for toggling between a frame having `alpha-background` values of 100 and 75:[^fn:3]

```emacs-lisp
(defun kb/toggle-window-transparency ()
  "Toggle transparency."
  (interactive)
  (let ((alpha-transparency 75))
    (pcase (frame-parameter nil 'alpha-background)
      (alpha-transparency (set-frame-parameter nil 'alpha-background 100))
      (t (set-frame-parameter nil 'alpha-background alpha-transparency)))))
```

In practice, this is what toggling transparency looks like:

{{< figure src="emacs-transparency.gif" caption="<span class=\"figure-number\">Figure 2: </span>Live-toggling of the `alpha-background` parameter." >}}

I had been using Håkon's patched version since becoming aware of it, and it's been perfect for me. My only gripe was that without the expertise of patching future releases of Emacs (Håkon wasn't updating his repository), I would inevitably be using an increasingly out-of-date version of Emacs.

However, I recently returned to check if there had been any updates to the repo. Though there were no new commits to the repository, I decided to check if there was work on mainline Emacs yet. I searched the repo for matches of `alpha-background`, and sure enough, I found a hit! **On January 30, 2022, [Po Lu silently announced the merging of the patch](https://github.com/emacs-mirror/emacs/commit/5c87d826201a5cae242ce5887a0aa7e24ad6f5ee)**. Accordingly, this means that _the `alpha-background` frame parameter is available to all builds after January 30, 2022_. I'll also note that do not know what went on behind-the-scenes to get this merged, so I'll make it clear that I am not attributing the development of this feature to only Håkon and Po Lu.

Of course, transparency in Emacs (alongside any other application on your system) is only possible if you are using a compositor (e.g. Picom). Regarding build flags, Håkon's patch supported transparency only with Emacs compiled with GTK3 and cairo support. I do not know for certain if this is still the case. If it is, then an Emacs built with the `--with-cairo` and `--with-x-toolkit=gtk3` flags is necessary (meaning this feature is only available on machines using `X Window`). However, [according to this Reddit comment thread](https://www.reddit.com/r/emacs/comments/v72tu6/comment/ibioym9/?utm_source=share&utm_medium=web2x&context=3), it seems like only cairo is necessary, while GTK3 is not necessary. That is, `--with-x-toolkit=no` works as well.


## Changelog {#changelog}

-   <span class="timestamp-wrapper"><span class="timestamp">June 8, 2022</span></span>
    -   Note that a compositor is necessary for transparency.
    -   Added statement about GTK3 perhaps not being necessary that links to a corresponding Reddit comment thread.

[^fn:1]: I work exclusively on a 14-inch laptop, so screen real-estate is valuable. I often use transparency to watch videos (e.g. YouTube, Netflix, lecture) while working on something in Emacs—I benefit from having the space of my entire screen.
[^fn:2]: The behavior of `alpha-background` matches that of `alpha`; that is, 100 is fully opaque while 0 is fully transparent.
[^fn:3]: The reason why my frame is not _fully_ opaque when `alpha-background` is 100 is because I've additionally set `alpha` to 98.
